package test;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;
/**
 * 回文检测
 * @author 周文华
 *
 */
public class SimpleCollectionTest {

	public static String[] strSplit(String str) {
		String[] strList = str.split("");
		return strList;
	}

	public static void main(String[] args) {
		Deque dq = new LinkedList<String>();
		Scanner in = new Scanner(System.in);
		String[] strs = strSplit(in.nextLine());
		String str1 = "";
		String str2 = "";

		for (int i = 0; i < strs.length; i++) {
			dq.add(strs[i]);
		}

		Iterator descendingiterator = dq.descendingIterator();
		Iterator iterator = dq.iterator();

		while (iterator.hasNext()) {
			str1 += iterator.next();
		
		}
		while (descendingiterator.hasNext()) {
			str2 += descendingiterator.next();
		}
		
		//System.out.println(str1);
		//System.out.println(str2);

		System.out.println(str1.equals(str2));
	}

}
