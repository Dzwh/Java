package test;

import java.util.HashMap;
import java.util.Map;

public class MapTest {
	public static void main(String[] args) {
		
		Map<String,Integer> map=new HashMap<>();
		
	
		map.put("1", 11);
		map.put("2", 22);
		map.put("3", 33);
		
		
		System.out.println(map.get("3").intValue());
		System.out.println(map);
		System.out.println(map.get("1"));
		System.out.println(map.containsKey("3"));
		
		map.keySet().forEach(key->System.out.println(key));//�����ֵ
		map.values().forEach(value->System.out.println(value));
		map.forEach((key,value)->System.out.println(key+"="+value));
	}

}
