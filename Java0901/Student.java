package Java0901;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
 * 
 * @author 周文华 201621123008
 *
 */
public class Student {
	private Long id;
	private String name;
	private int age;
	private Gender gender;// 枚举类型
	private boolean joinsACM; // 是否参加过ACM比赛
	/*static List<Student> studentList = new ArrayList<>();

	static {
		Student.studentList.add(new Student(001l, "张三", 18, Gender.MAN, true));
		Student.studentList.add(new Student(002l, "李四", 18, Gender.MAN, false));
		Student.studentList.add(null);
		Student.studentList.add(new Student(003l, "王二", 18, Gender.MAN, true));
		Student.studentList.add(null);
		Student.studentList.add(new Student(004l, "麻子", 18, Gender.MAN, false));
	}*/

	public Student(Long id, String name, int age, Gender gender, boolean joinsACM) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.joinsACM = joinsACM;
	}

	public static List<Student> search1(List<Student> studentList,Long id, String name, int age, Gender gender, boolean joinsACM) {
		List<Student> ACMStudents = new ArrayList<>();
		Student s = new Student(id, name, age, gender, joinsACM);
		for (int i = 0; i < studentList.size(); i++) {
			if (studentList.get(i) != null) {
				if (joinsACM == true) {
					if (studentList.get(i).equals(s)) {
						ACMStudents.add(s);

					}
				}
			}
		}
		return ACMStudents;

	}
	
	public static List<Student> search2(List<Student> studentList,Long id, String name, int age, Gender gender, boolean joinsACM){
		Student s = new Student(id, name, age, gender, joinsACM);
		List<Student> ACMStudents = studentList.stream().filter(e->e!=null&&e.equals(s)).collect(Collectors.toList());
		return ACMStudents;
		
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", age=" + age + ", gender=" + gender + ", joinsACM=" + joinsACM
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (joinsACM ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (age != other.age)
			return false;
		if (gender != other.gender)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (joinsACM != other.joinsACM)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public boolean isJoinsACM() {
		return joinsACM;
	}

	public void setJoinsACM(boolean joinsACM) {
		this.joinsACM = joinsACM;
	}

}
