package Java0901;

import java.util.*;

/**
 * Stream与Lambda
 * 
 * @author 周文华 201621123008
 * 
 *
 */
public class Main {

	public static void main(String[] args) {

		List<Student> studentList = new ArrayList<>();
		studentList.add(new Student(001l, "张三", 18, Gender.MAN, true));
		studentList.add(new Student(002l, "李四", 18, Gender.MAN, false));
		studentList.add(null);
		studentList.add(new Student(003l, "王二", 18, Gender.MAN, true));
		studentList.add(null);
		studentList.add(new Student(004l, "麻子", 18, Gender.MAN, false));
		
		System.out.println("周文华 201621123008");

		System.out.println(Student.search2(studentList,001l, "张三", 18, Gender.MAN, true));
	}

}
