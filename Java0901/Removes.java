package Java0901;

import java.util.*;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
/**
 * 删除元素的三种方法
 * @author 周文华
 *
 */
public class Removes {

	public static List<String> convertStringToList(String line) {

		List<String> list = new ArrayList<>();
		String[] strs = line.split(" ");

		for (String str : strs) {
			if (!str.equals(""))
				list.add(str);
		}
		return list;
	}

	public static void remove1(List<String> list, String str) {

		for (int i = 0; i < list.size(); i++)
			if (list.get(i).equals(str)) {
				list.remove(i);
				i -= 1;
			}

	}
	public static void remove2(List<String> list, String str) {
		for(int i=list.size()-1;i>=0;i--) {
			if (list.get(i).equals(str)) {
				list.remove(i);
			}
			
		}
	}
	
	public static void remove3(List<String> list, String str) {
		Iterator iterator=list.iterator();
		while(iterator.hasNext()) {
			if(iterator.next().equals(str)) {
				iterator.remove();
			}
		}
	}
	
	
	
	
	

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		while (sc.hasNextLine()) {
			List<String> list = convertStringToList(sc.nextLine());
			System.out.println(list);
			String word = sc.nextLine();
			remove1(list, word);
			System.out.println(list);
		}
		sc.close();
	}

}
