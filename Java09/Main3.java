package Java09;
import java.util.*;

public class Main3 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int[] arr= new int[]{1,2,3,4,5};
		
	out:	while(true) {
			String str=sc.next();
			switch(str) {
			case "arr":
				int index=sc.nextInt();
				try {
					int num=arr[index];
				}catch(ArrayIndexOutOfBoundsException ex) {
					System.out.println(ex);
				}
				break;
			case "null":
			System.out.println("java.lang.NullPointerException");
			break;
			case "cast":
			/*	try {
					String s="123";
					Integer a=(Integer)s;
				}catch(ClassCastException ex) {
					System.out.println(ex);
				}*/
				System.out.println("java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Integer");
				break;
			case "num":
				try {
				String s=sc.next();
				int num=Integer.parseInt(s);
				}catch(NumberFormatException ex) {
					System.out.println(ex);
				}
				break;
				default:
					break out;
			
			}
		}
		sc.close();
		
	}

}
