package Java09;

import java.util.*;

class FullStackException extends Exception {
	//...........

}

public class ArrayIntegerStack {
	int capacity;// 代表内部数组的大小
	int top;// 代表栈顶指针。栈空时，初始值为0。
	Integer[] arrStack;// 用于存放元素的数组

	public Integer push(Integer item) throws FullStackException {
		if (item == null) {
			return null;
		}
		if (top == capacity) {
			FullStackException e = new FullStackException();
			throw e;
		}
		arrStack[top] = item;
		top += 1;
		return item;

	}

	public Integer pop() throws EmptyStackException {
		if (top == 0) {
			EmptyStackException e = new EmptyStackException();
			throw e;

		} else {
			int a = arrStack[top - 1];
			top = top - 1;
			return a;
		}

	}

	public Integer peek() throws EmptyStackException {
		if (top == 0) {
			EmptyStackException e = new EmptyStackException();
			throw e;

		} else
			return arrStack[top - 1];

	}

}
