package Java09;
import java.util.*;

public class IllegalNameException extends RuntimeException{

	private String message;

	public IllegalNameException(String message) {
		super();
		this.message = message;
	}

	@Override
	public String toString() {
		return "IllegalNameException: " + message ;
	}
	
	
}
