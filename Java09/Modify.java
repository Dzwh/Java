package Java09;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.*;

public class Modify {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		byte[] content = null;
		FileInputStream fis = null;
		int bytesAvailabe = 0;
		
		while(true) {
		try {
			String path=sc.nextLine();
			fis = new FileInputStream(path);
			bytesAvailabe = fis.available();// 获得该文件可用的字节数
			if (bytesAvailabe > 0) {
				content = new byte[bytesAvailabe];// 创建可容纳文件大小的数组
				fis.read(content);// 将文件内容读入数组
				System.out.println(Arrays.toString(content));
				break;
			}
		}catch(FileNotFoundException e) {
			System.out.println("找不到文件"+fis+"，请重新输入文件名");
			continue;
		} catch (IOException e) {
			System.out.println("打开或读取文件失败!");
			System.out.println(e);
		}finally {
			try {
				System.out.println("关闭文件ing");
				fis.close();
			}catch(Exception e) {
				System.out.println("关闭文件失败!");
				
			}
		}
		}
		
		
	}

}
