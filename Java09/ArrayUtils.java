package Java09;

import java.util.*;

public class ArrayUtils {
	public static double findMax(double[] arr, int begin, int end) throws IllegalArgumentException {
		if (begin >= end) {
			IllegalArgumentException e = new IllegalArgumentException("begin:" + begin + " >= " + "end:" + end);

			throw e;
		}
		if (end > arr.length) {
			IllegalArgumentException e = new IllegalArgumentException("end:" + end + " > arr.length");
			throw e;

		}
		if (begin < 0) {
			IllegalArgumentException e = new IllegalArgumentException("begin:" + begin + " < 0");
			throw e;

		} else {
			double max = arr[begin];
			for (int i = begin + 1; i < end; i++) {
				if (arr[i] > max)
					max = arr[i];
			}
			return max;
		}

	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		double[] arr = new double[n];
		for (int i = 0; i < n; i++) {
			arr[i] = sc.nextInt();

		}

		out: while (true) {
			String a = sc.next();
			try {
				int a1 = Integer.parseInt(a);

			} catch (NumberFormatException e) {
				break out;
			}
			String b = sc.next();
			try {

				int b1 = Integer.parseInt(b);
			} catch (NumberFormatException e) {
				break out;
			}
			try {
				int a1 = Integer.parseInt(a);
				int b1 = Integer.parseInt(b);
			} catch (NumberFormatException e) {
				break out;
			}
			try {
				System.out.println(findMax(arr, Integer.parseInt(a), Integer.parseInt(b)));
			} catch (IllegalArgumentException e) {
				System.out.println(e);
			}
		}
		try {
			System.out.println(ArrayUtils.class.getDeclaredMethod("findMax", double[].class, int.class, int.class));
		} catch (Exception e1) {
		}
	}

}
