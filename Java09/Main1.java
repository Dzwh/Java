package Java09;

import java.util.*;

public class Main1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while (sc.hasNext()) {
			String choice = sc.next();
			try {
				if (choice.equals("number"))
					throw new NumberFormatException();
				else if (choice.equals("illegal")) {
					throw new IllegalArgumentException();
				} else if (choice.equals("except")) {

					throw new Exception();

				} else
					break;
			} catch (NumberFormatException ex) {
				ex.printStackTrace();
				ex.getMessage();
				System.out.println("NumberFormatException");
				System.out.println(ex);

			} catch (IllegalArgumentException ex) {
				System.out.println("IllegalArgumentException");
				System.out.println(ex);

			} catch (Exception ex) {
				System.out.println("Exception");
				System.out.println(ex);
			}

			
		}
		sc.close();
	}

}
