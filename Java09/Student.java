package Java09;

public class Student {
	private String name;
	private int score;

	@Override

	public String toString() {
		return "Student [name=" + name + ", score=" + score + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) throws IllegalNameException {
		String[] strs = name.split("");
		String str = strs[0];
		boolean b = str.matches("\\d");
		if (b) {
			IllegalNameException ex = new IllegalNameException(
					"the first char of name must not be digit, name=" + name);
			throw ex;

		} else {
			this.name = name;
		}
		/*
		 * try { int a=Integer.parseInt(strs[0]); IllegalNameException ex=new
		 * IllegalNameException("the first char of name must not be digit, name="+name);
		 * throw ex; }catch(NumberFormatException e) { this.name = name; }
		 */

	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int addScore(int score) throws IllegalScoreException {
		if (score > 100 || score < 0) {
			IllegalScoreException ex = new IllegalScoreException("score out of range, score=" + score);
			throw ex;
		} else {
			setScore(score);
			return score;
		}

	}

}
