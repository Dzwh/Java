package Java09;

import java.util.*;

public class Main4 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		
		while (true) {
			boolean flag = true;
			String str = sc.nextLine();
			if (!str.equals("new"))
				break;

			Student stu = new Student();
			String[] strs = sc.nextLine().split(" ");
			
			if (strs.length < 2)
				System.out.println("java.util.NoSuchElementException");
			if (strs.length == 2) {
				try {
					stu.setName(strs[0]);
					

				} catch (IllegalNameException ex) {
					flag = false;
					System.out.println(ex);
				}
                   try {
                	   stu.addScore(Integer.parseInt(strs[1]));
                   }
				catch (IllegalScoreException ex) {
					flag = false;
					System.out.println(ex);
				}
					
				
				if (flag)
					System.out.println(stu);
				

			}
		}
		
		sc.close();
		System.out.println("scanner closed");	
		
	}

}
