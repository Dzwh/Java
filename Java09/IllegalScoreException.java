package Java09;
import java.util.*;

public class IllegalScoreException extends Exception{
	private String massage;

	public IllegalScoreException(String massage) {
		super();
		this.massage = massage;
	}

	@Override
	public String toString() {
		return "IllegalScoreException: " + massage ;
	}
	
	

}
