package Java09;

import java.util.*;

public class Main2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = Integer.parseInt(sc.nextLine());
		int[] arr = new int[n];

		for (int i = 0; i < n; i++) {

			try {
				String str = sc.nextLine();
				int num = Integer.parseInt(str);
				arr[i] = num;
			} catch (java.lang.NumberFormatException ex) {
				System.out.println(ex);
				i-=1;
				continue;
			}
		}
		sc.close();
		System.out.println(Arrays.toString(arr));
		
	}

}
