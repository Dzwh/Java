package JavaLearn08;

import java.util.*;

public class Main5 {
	public static void main(String[] args) {
		Map<String, List> wordMap = new TreeMap<>();//存放单词及对所在的行数，key为单词，list存放所在行的信息
		Map<Integer, String> rowMap = new TreeMap<>();//存放每一行对应的单词，key为行数
		Scanner sc = new Scanner(System.in);
		int row = 1;
		while (true) {
			String str = sc.nextLine();
			if (str.equals("!!!!!"))
				break;

			rowMap.put(row++, str);

			String[] strs = str.split(" ");
			for (String temp : strs) {
				
				if (!temp.equals("")) {
					if (wordMap.containsKey(temp)) {
						List tempList = wordMap.get(temp);
						tempList.add(row - 1);
						wordMap.replace(temp, wordMap.get(temp), tempList);

					} else {
						List<Integer> rows = new ArrayList<>();
						rows.add(row - 1);
						wordMap.put(temp, rows);
					}

				}
			}

		}
		//输出
		wordMap.forEach((key,value)->System.out.println(key+"="+value));
		
		while(true) {
			List<Integer> commonList=new ArrayList<>();
			List<Integer> tempList=new ArrayList<>();
			String word=sc.nextLine();
			String[] words=word.split(" ");
			for(int i=0;i<words.length;i++) {
				if(wordMap.containsKey(words[i])) {
					tempList=wordMap.get(words[i]);
					if(i==0) {
						commonList=tempList;
					}
					ArrayList<Integer> son=new ArrayList<>();
					for(int j=0;j<tempList.size();j++) {
						for(int k=0;k<commonList.size();k++) {
							if(commonList.get(k).equals(tempList.get(j)))
								son.add(tempList.get(j));
						}
					}
					commonList.clear();
					commonList=son;
					
					
				}else {
					commonList.clear();
					break;
				}
			}
			
			if(commonList.isEmpty())
				System.out.println("found 0 results");
			else {
				System.out.println(commonList);
				for(Integer r:commonList)
					System.out.println("line "+r+":"+rowMap.get(r));
					
			}
				
		}
		

	}

}
