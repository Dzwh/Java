package JavaLearn08;

public interface IntegerStack {
	public Integer push(Integer item); //如item为null，则不入栈直接返回null。如栈满，也返回null.
	public Integer pop();             //出栈，如为空，则返回null.
	public Integer peek();           //获得栈顶元素，如为空，则返回null.
	public boolean empty();         //如为空返回true
	public int size();             //返回栈中元素数量

}
