package JavaLearn08;

import java.util.ArrayList;
import java.util.List;

public class ArrayListIntegerStack implements IntegerStack {

	private List<Integer> list;
	private int n;

	public List<Integer> getList() {
		return list;
	}

	public void setList(List<Integer> list) {
		this.list = list;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	@Override
	public String toString() {
		return list.toString();
	}

	@Override
	public Integer push(Integer item) {
		if (item.equals(null) && list.size() > n)
			return null;
		else
			list.add(item);
		return item;

	}

	@Override
	public Integer pop() {
		if (list.size() == 0)
			return null;
		else {
			Integer popValue = list.get(list.size() - 1);
			list.remove(list.size() - 1);
			return popValue;
		}

	}

	@Override
	public Integer peek() {
		if (list.size() == 0)
			return null;
		else
			return list.get(list.size() - 1);
	}

	@Override
	public boolean empty() {
		return list.size() == 0;
	}

	@Override
	public int size() {
		return list.size();
	}

}
