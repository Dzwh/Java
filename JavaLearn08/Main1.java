package JavaLearn08;

import java.util.ArrayList;
import java.util.Scanner;

public class Main1 {
	public static void main(String[] args) {
		ArrayListIntegerStack listStack=new ArrayListIntegerStack();
		Scanner sc=new Scanner(System.in);
		
		listStack.setN(sc.nextInt());
		listStack.setList(new ArrayList<Integer>());
		
		for(int i=0;i<listStack.getN();i++)
			System.out.println(listStack.push(sc.nextInt()));
		int n=sc.nextInt();
		System.out.print(listStack.peek()+",");
		System.out.print(listStack.peek()==null);
		System.out.println(","+listStack.getList().size());
		System.out.println(listStack.getList().toString());
		
		for(int i=0;i<n;i++) {
			System.out.println(listStack.pop());	
		}
		
		System.out.print(listStack.peek()+",");
		System.out.print(listStack.peek()==null);
		System.out.println(","+listStack.getList().size());
		System.out.println(listStack.getList().toString());
	}

}
