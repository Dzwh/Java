package JavaLearn08;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Main2 {

	public static void main(String[] args) {
		Map<String, Integer> map = new TreeMap<>();
		Scanner sc = new Scanner(System.in);
		String temp = null;

		while (true) {
			temp = sc.nextLine();
			if (temp.equals("!!!!!"))
				break;

			String[] strs = temp.split(" ");
			for (String str : strs) {
				if (!str.equals(" ")&&(!str.equals(""))) {
					if (!map.containsKey(str)) {
						map.put(str, 1);
					} else {
						map.replace(str, map.get(str).intValue(), map.get(str).intValue() + 1);
					}
				}
			}
		}

		System.out.println(map.size());
		System.out.println(map);

		Collection values = map.keySet();
		Iterator iterator = values.iterator();
		for (int i = 0; i < 10; i++)
			System.out.println(iterator.next());

	}
}
