package JavaLearn08;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

interface GeneralStack<E>{
	public E push(E item); 
	public E pop();             
	public E peek();           
	public boolean empty();         
	public int size();             
}
class ArrayListGeneralStack<E> implements GeneralStack<E> {
	private List<E> list;
	private int n;

	public List<E> getList() {
		return list;
	}

	public void setList(List<E> list) {
		this.list = list;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	@Override
	public String toString() {
		return list.toString();
	}

	@Override
	public E push(E item) {
		if (item.equals(null) && list.size() > n)
			return null;
		else
			list.add(item);
		return item;

	}

	@Override
	public E pop() {
		if (list.size()==0)
			return null;
		else {
			E popValue = list.get(list.size() - 1);
			list.remove(list.size() - 1);
			return popValue;
		}

	}

	@Override
	public E peek() {
		if (list.isEmpty())
			return null;
		else
			return list.get(list.size() - 1);
	}

	@Override
	public boolean empty() {
		return list.size() == 0;
	}

	@Override
	public int size() {
		return list.size();
	}



	
}

class Car{
	private int id;
	private String name;
	@Override
	public String toString() {
		return "Car [id=" + id + ", name=" + name + "]";
	}
	public Car(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
	
	
	
}
public class Main6 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		
		while(true) {
			
			String str=sc.next();
			if(str.equals("quit")) {
				sc.close();
				break;}
			if(str.equals("Integer")) {
				ArrayListGeneralStack<Integer> integerStack=new ArrayListGeneralStack<Integer>();
				integerStack.setList(new ArrayList<Integer>());
				int stackNumber=sc.nextInt();
				int popNumber=sc.nextInt();
				integerStack.setN(stackNumber);
				int sum=0;
				System.out.println("Integer Test");
				for(int i=0;i<stackNumber;i++) {
					integerStack.getList().add(sc.nextInt());
					System.out.println("push:"+integerStack.peek());
				}
				for(int i=0;i<popNumber;i++) {
					System.out.println("pop:"+integerStack.pop());
				}
				System.out.println(integerStack);
				for(Integer temp:integerStack.getList())
					sum+=temp;
				System.out.println("sum="+sum);
				System.out.println(integerStack.getClass().getInterfaces()[0]);
				
			}
			if(str.equals("Double")) {
				ArrayListGeneralStack<Double> integerStack=new ArrayListGeneralStack<Double>();
				integerStack.setList(new ArrayList<Double>());
				int stackNumber=sc.nextInt();
				int popNumber=sc.nextInt();
				integerStack.setN(stackNumber);
				double sum=0.0;
				System.out.println("Double Test");
				for(int i=0;i<stackNumber;i++) {
					integerStack.getList().add(sc.nextDouble());
					System.out.println("push:"+integerStack.peek());
				}
				for(int i=0;i<popNumber;i++) {
					System.out.println("pop:"+integerStack.pop());
				}
				System.out.println(integerStack);
				for(Double temp:integerStack.getList())
					sum+=temp;
				System.out.println("sum="+sum);
				System.out.println(integerStack.getClass().getInterfaces()[0]);
				
				
			}
			if(str.equals("Car")) {
				ArrayListGeneralStack<Car> integerStack=new ArrayListGeneralStack<Car>();
				integerStack.setList(new ArrayList<Car>());
				int stackNumber=sc.nextInt();
				int popNumber=sc.nextInt();
				integerStack.setN(stackNumber);
				System.out.println("Car Test");
				for(int i=0;i<stackNumber;i++) {
					integerStack.getList().add(new Car(sc.nextInt(),sc.next()));
					System.out.println("push:"+integerStack.peek());
				}
				for(int i=0;i<popNumber;i++) {
					System.out.println("pop:"+integerStack.pop());
				}
				System.out.println(integerStack);
				for(int i=0;i<integerStack.getList().size();i++)
					System.out.println(integerStack.pop().getName());
				System.out.println(integerStack.getClass().getInterfaces()[0]);
				
				
				
			}
		}
	}
	
	

	
}
