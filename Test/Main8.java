package Test;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class MyTask implements Runnable{

	CountDownLatch latch;
	
	

	public MyTask(CountDownLatch latch) {
		super();
		this.latch = latch;
	}



	@Override
	public void run() {
		System.out.println("begin");
	/*	try {
			System.out.println("begin");
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		//latch.countDown();
		
	}
	
}
public class Main8 {
    public static void main(String[] args) throws InterruptedException {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNextInt()){
            int n = sc.nextInt();//任务数
            int poolSize = sc.nextInt();//最多使用的线程数
        
            CountDownLatch latch=new CountDownLatch(n);
            ExecutorService exec=Executors.newFixedThreadPool(poolSize);
            for(int i=0;i<n;i++) {
            	exec.execute(new MyTask(latch));
            	latch.countDown();
            }
            
            exec.shutdown();
            latch.await();//主线程阻塞，直到前面线程都执行完，再执行后面的代码
            /*这里是系统的其他代码*/
        }
        sc.close();
    }
}
