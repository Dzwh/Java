package Test;

import java.util.Scanner;
/*这里放置你的代码*/

class MonitorTask implements Runnable {

	private volatile boolean flag = false;
	private String word;

	public void run() {

		while (!flag) {
			if (word != null) {
				if (word.contains("alien")) {
					System.out.println(Thread.currentThread().getName() + " found alien in " + word);
				}
				word = null;
			}
		}
		System.out.println(Thread.currentThread().getName() + " stop");
	}

	public void stopMe() {

		flag = true;

	}

	public void sendWord(String word) {
		this.word = word;
	}
}

public class Main2 {
	public static void main(String[] args) throws InterruptedException {
		MonitorTask task1 = new MonitorTask();
		Thread t1 = new Thread(task1);
		t1.start();
		Scanner sc = new Scanner(System.in);
		while (sc.hasNext()) {
			String word = sc.next();
			if (word.equals("stop")) {
				task1.stopMe();
			} else if (word.equals("quit")) {
				break;
			} else {
				if (t1.isAlive())
					task1.sendWord(word);
			}
			Thread.sleep(10);
		}
		System.out.println(t1.isAlive());
		task1.stopMe();
		sc.close();
	}
}
