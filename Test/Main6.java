package Test;

import java.util.*;

class Repo{
	private String[] strs=null;
	private int i=0;
	private volatile ArrayList<String> list=new ArrayList<>();
	
	public Repo(String items) {
		 strs=items.split("\\s+");
		 for(int i=0;i<strs.length;i++) {
			 list.add(strs[i]);
		 }
	}
	public int getSize(){
		return list.size();
	}
	public synchronized void doTasks(){
		while(list.size()!=0) {
			System.out.println(Thread.currentThread().getName()+" finish "+list.get(i));
			list.remove(i);
			if(0==list.size())
				break;
			notify();
			try {
				wait(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}


}
class Worker1 implements Runnable{
	Repo repo;

	public Worker1(Repo repo) {
		super();
		this.repo = repo;
	}

	@Override
	public void run() {
		
		repo.doTasks();
			
		
	}
}

class Worker2 implements Runnable{
	Repo repo;

	public Worker2(Repo repo) {
		super();
		this.repo = repo;
	}

	@Override
	public void run() {
	
		repo.doTasks();
		
	}
}
public class Main6 {
    public static void main(String[] args) throws InterruptedException {
        Scanner sc = new Scanner(System.in);
        Repo repo = new Repo(sc.nextLine());
        Thread t1 = new Thread(new Worker1(repo));
        Thread t2 = new Thread(new Worker2(repo));
        t1.start();
        Thread.yield();
        t2.start();
        sc.close();
    }
}
