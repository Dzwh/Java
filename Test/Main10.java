package Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

class CalculateTask implements Callable<Integer> {

	private int n;

	public CalculateTask(int n) {
		super();
		this.n = n;
	}

	@Override
	public Integer call() throws Exception {
		int front = 0;
		int tail = 1;
		int result = 0;
		if (n == 0 || n == 1)
			return n;
	
		else {
			for (int i = 1; i < n; i++) {
				result = front + tail;
				front = tail;
				tail = result;
			}
			return result;
		}
		
	}

}

public class Main10 {

	public static void main(String[] args) throws InterruptedException {
		ExecutorService exec = Executors.newCachedThreadPool();
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		List<CalculateTask> taskList = new ArrayList<CalculateTask>();
		List<Future<Integer>> results = new ArrayList<Future<Integer>>();
		int sum = 0;
		sc.close();
		for(int i=0;i<=n;i++)
			taskList.add(new CalculateTask(i));
		
		for(int i=0;i<taskList.size();i++) {
			results.add( (Future<Integer>) exec.submit((Callable<Integer>) taskList.get(i)));
		}
		exec.shutdown();
		
		for(int i=0;i<results.size();i++)
			try {
				sum+=results.get(i).get();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		System.out.println("sum="+sum);
	}
}