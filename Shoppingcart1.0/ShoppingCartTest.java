package AboutShopping;

import java.util.Scanner;

/**
 * 测试类
 * 
 * @author 周文华
 *
 */
public class ShoppingCartTest {
	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);

		System.out.println("由于您初次登录，请先创建账户");
		System.out.println("请输入您的账户名");
		String userId = in.next();
		System.out.println("请输入您的密码");
		String userPassword = in.next();
		User user = new User(userId, userPassword, new ShoppingCart());
		if (user != null) {
			System.out.println("账户创建成功");
		}
		System.out.println(String.format("您的用户名:%s  您的密码:%s", user.getUserId(), user.getUserPassword()));

		boolean pass;
		do {
			System.out.println("请输入您的账户和密码进行登录");
			String loginId=in.next();
			String loginPassword = in.next();
			pass = Storage.login(loginId, loginPassword, user);
			if (pass == false)
				System.out.println("帐号密码不匹配");
		} while (pass != true);
		if (pass == true)
			System.out.println("登录成功");

		System.out.println("本商城商品清单:");
		Storage.showStorage();

		System.out.println("您可以输入商品种类进行搜索,以便快速浏览您想要的商品,输入exit退出搜索");
		while (true) {
			String key = in.next();
			if (key.equals("exit"))
				break;
			Storage.searchGoodsAccordingToKind(key);
		}

		System.out.println("如有您想要的商品,您可输入商品编号,把该商品加入购物车，输入exit退出添加");
		while (true) {
			String str = in.next();
			if (str.equals("exit"))
				break;
			int id = Integer.parseInt(str);
			int b = user.getUserShoppinngCart().addGoods(id);
			if (b == 1) {
				System.out.println("添加成功");
			}
			if (b == 0) {
				System.out.println("由于库存不足，商品添加失败");
			}
			if (b == -1)
				System.out.println("该商品在本商城暂停销售");
		}

		System.out.println("购物清单");
		user.getUserShoppinngCart().showGoosList();
		System.out.println(String.format("总计:%.2f元", user.getUserShoppinngCart().allTotalMoney()));

		System.out.println("您想对购物车进行修改吗?");
		System.out.println("是:Y");
		System.out.println("否:N");
		String input = in.next();
		if (input.equals("Y")) {
			System.out.println("Clean:C");
			System.out.println("Dlete:D");
			System.out.println("Modify:M");
			System.out.println("输入exit退出");
			String choice = "replace";
			while (!choice.equals("exit")) {
				choice = in.nextLine();
				switch (choice) {
				case "M":
					System.out.println("输入商品编号及商品数量进行修改");
					int b = user.getUserShoppinngCart().modifyGoods(in.nextInt(), in.nextInt());
					if (b == 1)
						System.out.println("修改成功");
					if (b == 0) {
						System.out.println("由于库存不足，修改失败");
					}
					if (b == -1) {
						System.out.println("该商品在本商城暂停销售");
					}
					break;
				case "D":
					System.out.println("请输入要删除商品的编号");
					boolean b1 = user.getUserShoppinngCart().deleteGoods(in.nextInt());
					if (b1 == true)
						System.out.println("删除成功");
					else {
						System.out.println("删除失败,该商品不再购物车");
					}
					break;
				case "C":
					boolean b2 = user.getUserShoppinngCart().clearShoppingCart();
					if (b2)
						System.out.println("购物车已清空");
					break;
				}
			}

		} else {
			System.out.println("谢谢光临!");
			return;
		}
		System.out.println("购物清单");
		user.getUserShoppinngCart().showGoosList();
		System.out.println(user.getUserShoppinngCart().allTotalMoney()+"元");
		System.out.println("谢谢光临!");
	}
}
