package AboutShopping;
/**
 * 用户类
 * @author 周文华
 *
 */
public class User {
	private String userId;//账户
	private String userPassword;//密码
	private String userAddress;//用户地址
	private ShoppingCart userShoppinngCart;//用户的购物车
	
	
	public User(String userId, String userPassword, ShoppingCart userShoppinngCart) {
		super();
		this.userId = userId;
		this.userPassword = userPassword;
		this.userShoppinngCart = userShoppinngCart;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserPassword() {
		return userPassword;
	}
	
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	
	public ShoppingCart getUserShoppinngCart() {
		return userShoppinngCart;
	}
	
	public void setUserShoppinngCart(ShoppingCart userShoppinngCart) {
		this.userShoppinngCart = userShoppinngCart;
	}
	

}
