package AboutShopping;
/**
 * 登录类
 * 
 *
 */
final public class Login {
	public static String loginId;
	public static String loginPassword;

	private Login() {
		super();
	}

	public String getLoginId() {
		return loginId;
	}

	public static void setLoginId(String loginId) {
		loginId = loginId;
	}

	public String getLoginPassword() {
		return loginPassword;
	}

	public static void setLoginPassword(String loginPassword) {
		loginPassword = loginPassword;
	}

	//对用户输入的账户信息进行匹配
	public static boolean matchInformation(User user) {
		if (user.getUserId().equals(loginId) && user.getUserPassword().equals(loginPassword))
			return true;
		else
			return false;

	}

}
