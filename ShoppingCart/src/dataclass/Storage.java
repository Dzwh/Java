/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataclass;

import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.List;
import java.util.*;
 /**
 * @author 周文华
 */
public class Storage {
    	//定义为static
	public static Map<Integer, GoodsItem> storageGoodsList = new TreeMap<Integer, GoodsItem>();

	// 对仓库初始化
	static {
		storageGoodsList.put(001, new GoodsItem(new Goods(001, "疯狂的Java", 53.3, "book"), 5));
		storageGoodsList.put(002, new GoodsItem(new Goods(002, "Head Frist Java", 83.1, "book"), 6));
		storageGoodsList.put(003, new GoodsItem(new Goods(003, "Thinking in Java", 63.8, "book"), 7));
		storageGoodsList.put(004, new GoodsItem(new Goods(004, "Java从入门到放弃？", 2.33, "book"), 8));
		storageGoodsList.put(005, new GoodsItem(new Goods(005, "pants", 23.3, "clothing"), 8));
		storageGoodsList.put(006, new GoodsItem(new Goods(006, "T-Shirt", 23.3, "clothing"), 4));
		storageGoodsList.put(007, new GoodsItem(new Goods(007, "coat", 23.3, "clothing"), 2));
		storageGoodsList.put(010, new GoodsItem(new Goods(010, "dress", 23.3, "clothing"), 6));

	}
        
    	public static List<GoodsItem> searchGoodsAccordingToKind(String key) {
		Collection<GoodsItem> goodsItems = Storage.storageGoodsList.values();
		Iterator iterator = goodsItems.iterator();
		List<GoodsItem> keyList = new ArrayList<GoodsItem>();

		while (iterator.hasNext()) {
			GoodsItem goodsItem = (GoodsItem) iterator.next();
			Goods goods = goodsItem.getGood();
			if (goods.getGoodCategory().equals(key)) {

				keyList.add(goodsItem);
			}
		}
		return keyList;

	}
}