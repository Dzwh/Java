/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataclass;

/**
 *
 * @author 周文华
 */
public class Goods  {
	private Integer goodId; // 商品编号
	private String goodName; // 商品名称
	private double goodprice; // 商品价格
	private String goodCategory; // 商品种类

	public Goods() {
		super();
	}

	public Goods(int goodId, String goodName, double goodprice, String goodCategory) {
		super();
		this.goodId = goodId;
		this.goodName = goodName;
		this.goodprice = goodprice;
		this.goodCategory = goodCategory;
	}

	@Override
	public String toString() {
		return "Goods [goodId=" + goodId + ", goodName=" + goodName + ", goodprice=" + goodprice + ", goodCategory="
				+ goodCategory + "]";
	}

	public Integer getGoodId() {
		return goodId;
	}

	public void setGoodId(Integer goodId) {
		this.goodId = goodId;
	}

	public String getGoodName() {
		return goodName;
	}

	public void setGoodName(String goodName) {
		this.goodName = goodName;
	}

	public double getGoodprice() {
		return goodprice;
	}

	public void setGoodprice(double goodprice) {
		this.goodprice = goodprice;
	}

	public String getGoodCategory() {
		return goodCategory;
	}

	public void setGoodCategory(String goodCategory) {
		this.goodCategory = goodCategory;
	}

}

