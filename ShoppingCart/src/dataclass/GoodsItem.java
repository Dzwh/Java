/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataclass;

/**
 *
 * @author 周文华
 */
public class GoodsItem  {
	private Goods good;//对应的具体商品
	private Integer count;//对应商品数量
	
	
	public GoodsItem() {
		super();
	}

	public GoodsItem(Goods good, int count) {
		super();
		this.good = good;
		this.count = count;
	}

	public Goods getGood() {
		return good;
	}

	public void setGood(Goods good) {
		this.good = good;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	//计算该商品的总价格
	public double totalMoney() {
		return this.good.getGoodprice()*count;
	}

	
	@Override
	public String toString() {
		return "GoodsItem [goodsId="+good.getGoodId()+ ", goodName=" + good.getGoodName()+ ", goodprice=" + good.getGoodprice()  +", goodCategory="
				+ good.getGoodCategory() +",goodsNumber= "+this.count+"]"; 
				
	}
	
	
	
	

}
