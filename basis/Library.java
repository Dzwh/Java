package basis;

import java.util.*;;

public class Library implements Universal {
	private Map<String, BookItem> libraryMap = new TreeMap<>();

	public <T> void print(Collection<T> value) {
		Iterator iterator = value.iterator();
		while (iterator.hasNext())
			System.out.println(iterator.next());

	}

	public void showBooks() {
		Collection<BookItem> BooksItems = libraryMap.values();
		Iterator<BookItem> iterator = BooksItems.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
			;
		}
	}

	private boolean isExit(String bookId) {
		return libraryMap.containsKey(bookId);
	}

	public int borrowBooks(String bookId) {
		Collection<Set<BookItem>> setBooksItems = StackRoom.booksMap.values();
		Iterator<Set<BookItem>> iterator = setBooksItems.iterator();
		while (iterator.hasNext()) {
			Set<BookItem> setBookItem = iterator.next();
			Iterator setBooks = setBookItem.iterator();
			while (setBooks.hasNext()) {
				BookItem bookItem = (BookItem) setBooks.next();
				Book book = bookItem.getBook();
				if (book.getBookId().equals(bookId)) {
					boolean result = isExit(bookId);
					if (result) {
						BookItem a = libraryMap.get(bookId);
						if (a.getCount() >= bookItem.getCount()) {
							return 0; // 该书以及全部借出
						} else {
							a.setCount(a.getCount() + 1);
							return 1; // 借书成功
						}
					}
				}
			}

		}
		return -1; // bookId有误，不存在该书

	}

	public boolean returningBooks(String bookId) {
		if (libraryMap.containsKey(bookId)) {
			libraryMap.remove(bookId);
			return true;
		}else {
			return false;
		}

	}

}
