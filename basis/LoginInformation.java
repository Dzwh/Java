package basis;

public class LoginInformation {
	private String userId;
	private String password;
	
	public String getId() {
		return userId;
	}
	public void setId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
