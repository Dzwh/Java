package basis;

import java.text.SimpleDateFormat;
import java.util.*;

public abstract class Book {

	private String bookId;
	private String bookName;
	private String author;
	private String category;
	private GregorianCalendar date;

	public Book(String bookId, String bookName, String author, String category, GregorianCalendar date) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.author = author;
		this.category = category;
		this.date = date;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public GregorianCalendar getDate() {
		return date;
	}

	public void setData(GregorianCalendar date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", bookName=" + bookName + ", author=" + author + ", category=" + category
				+ ", date=" + date.get(GregorianCalendar.YEAR)+"-"+date.get(GregorianCalendar.MONTH)+"-"+ date.get(GregorianCalendar.DAY_OF_MONTH)+ "]";
	}

}
