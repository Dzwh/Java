package basis;

import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.*;

public class Main {
	public static void main(String[] args) {

		// 初始化用户仓库，存储用户信息
		UserStorage users = new UserStorage();
		User user = null;

		Scanner sc = new Scanner(System.in);

		System.out.println("1.注册");
		System.out.println("2.登录");
		System.out.println("0.退出系统");
		String choice = sc.next();

		if (choice.equals("1")) {
			System.out.println("填写个人信息：");
			System.out.println("职业：");
			String choiceProfession = sc.next();
			System.out.println("工号：");
			long userNo = sc.nextLong();
			System.out.println("姓名：");
			String name = sc.next();
			while (true) {
				System.out.println("帐号：");
				String userId = sc.next();
				System.out.println("密码：");
				String password = sc.next();
				user = Entry.writePersonalInformation(choiceProfession, userNo, name, userId, password);
				boolean a = Entry.register(user, users);
				if (a) {
					System.out.println("注册成功");
					System.out.println("跳转为自动登录");
					break;
				} else {
					System.out.println("注册失败：该帐号已存在");
					System.out.println("请重新注册");
					continue;
				}
			}
		}
		if (choice.equals("2")) {
			out: while (true) {
				String userId = sc.next();
				String password = sc.next();
				int a = Entry.login(userId, password, users);
				switch (a) {
				case 1:
					System.out.println("登录成功");
					break out;
				case 0:
					System.out.println("帐号密码不匹配");
					System.out.println("请重新输入");
					continue out;
				case -1:
					System.out.println("该帐号不存在");
					System.out.println("请重新输入");
					continue out;
				}
			}
		}
		if (choice.equals("0")) {
			System.exit(0);
		}

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("1.借书");
		System.out.println("2.还书");
		System.out.println("3.我的图书馆");
		System.out.println("0.退出系统");
		while (true) {
			String choice1 = sc.next();
			if (choice1.equals("1")) {
				System.out.println("图书馆书目信息");
				StackRoom.showBooks();

				while (true) {
					System.out.println("请输入书籍的种类进行搜索,输入exit退出");
					String key = sc.next();
					if (key.equals("exit"))
						break;
					StackRoom.print(StackRoom.searchBooks(key));
				}
				while (true) {
					System.out.println("请输入bookId借阅书籍,输入exit退出");
					String bookId = sc.next();
					if (bookId.equals("exit"))
						break;
					int a = user.getLibraly().borrowBooks(bookId);
					if (a == 1)
						System.out.println("借书成功");
					if (a == 0)
						System.out.println("馆藏不足");
					if (a == -1)
						System.out.println("图书馆没有该书目信息");
				}

			}
			if (choice1.equals("2")) {
				String bookId=sc.next();
				boolean a=user.getLibraly().returningBooks(bookId);
				if(a)
					System.out.println("还书成功");
				else {
					System.out.println("该书已经归还");
				}

			}
			if (choice1.equals("0"))
				System.exit(0);
			if(choice1.equals("3")) {
				user.showUser();
			}
		}

	}
}
