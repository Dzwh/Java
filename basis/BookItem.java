package basis;

public class BookItem {
	private Book book;
	private int count;
	public BookItem(Book book, int count) {
		super();
		this.book = book;
		this.count = count;
	}
	@Override
	public String toString() {
		return "BookItem [book=" + book + ", count=" + count + "]";
	}
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	

}
