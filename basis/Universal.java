package basis;

import java.util.Collection;

public interface Universal {

	public <T> void print(Collection<T> value);
}
