package basis;

import java.util.*;

public class Entry {

	public static User professionJudge(String profession) {
		if (profession.equalsIgnoreCase("Administrator"))
			return new Administrator();
		if (profession.equalsIgnoreCase("Student"))
			return new Student();
		if (profession.equalsIgnoreCase("Teacher"))
			return new Teacher();
		else
			return null;

	}

	/**
	 * 填写注册前的基本信息。
	 * 
	 * @param userNo
	 * @param name
	 * @param userId
	 * @param password
	 * @return
	 */
	public static <T extends User> T writePersonalInformation(String profession, Long userNo, String name,
			String userId, String password) {
		User user = professionJudge(profession);
		if (user != null) {
			LoginInformation info = new LoginInformation();
			info.setId(userId);
			info.setPassword(password);
			user.setUserNo(userNo);
			user.setUserName(name);
			user.setLoginInformation(info);
			user.setLibraly(new Library());
			return (T) user;
		}
		return null;
		

	}

	/**
	 * 将user加入users
	 * 
	 * @param user
	 * @param set
	 * @return
	 */
	public static <T extends User> boolean register(User user, UserStorage storage) {
		if(user==null)
			return false;
		boolean result = storage.isExit(user.getLoginInformation().getId());
		if (result) {
			return false;
		} else {
			storage.addUser(user);
			return true;
		}
	}

	/**
	 * 登录
	 * 
	 * @param name
	 * @param password
	 * @param users
	 * @return
	 */

	public static int login(String name, String password, UserStorage users) {
		boolean result = users.isExit(name);
		if (result) {
			int flag = 0;
			Iterator<User> iterator = users.getUserSet().iterator();
			while (iterator.hasNext()) {
				if (password.equals(iterator.next().getLoginInformation().getPassword()))
					flag = 1; // 登录成功
				else
					flag = 0; // 帐号密码不匹配
			}
			return flag;

		} else {
			return -1; // 帐号不存在

		}

	}

}
