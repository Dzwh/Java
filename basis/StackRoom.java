package basis;

import java.util.*;

/**
 * 类别对应Set:存放该类别书本的条目信息
 * 
 * @author 周文华
 *
 */
public class StackRoom {

	public static Map<String, Set<BookItem>> booksMap = new HashMap<>();

	static {

		Set<BookItem> economicalBook = new HashSet<>();
		economicalBook.add(new BookItem(
				new EconomicalBook("001", "货币金融学", "弗雷德克里S·米什金", "经济", new GregorianCalendar(2001, 6, 1)), 5));
		economicalBook.add(
				new BookItem(new EconomicalBook("002", "博弈论与信息学", "	张维迎", "经济", new GregorianCalendar(2001, 6, 1)), 5));
		economicalBook.add(
				new BookItem(new EconomicalBook("003", "国富论", "亚当.斯密", "经济", new GregorianCalendar(2001, 6, 1)), 5));
		economicalBook.add(new BookItem(
				new EconomicalBook("004", "牛奶可乐经济学", "弗雷德克里S·米什金", "经济", new GregorianCalendar(2001, 6, 1)), 5));
		booksMap.put("经济", economicalBook);

		Set<BookItem> militaryBook = new HashSet<>();
		militaryBook
				.add(new BookItem(new MilitaryBook("005", "孙子兵法", "孙子", "军事", new GregorianCalendar(2001, 6, 1)), 5));
		militaryBook.add(new BookItem(
				new MilitaryBook("006", "第二次世界大战史", "马丁·吉尔伯特 ", "军事", new GregorianCalendar(2001, 6, 1)), 5));
		militaryBook
				.add(new BookItem(new MilitaryBook("007", "太平洋战争", "青梅煮雨", "军事", new GregorianCalendar(2001, 6, 1)), 5));
		militaryBook
				.add(new BookItem(new MilitaryBook("0010", "梦残干戈", "黄朴民", "军事", new GregorianCalendar(2001, 6, 1)), 5));
		booksMap.put("军事", militaryBook);

	}

	/**
	 * 打印
	 * 
	 * @param value
	 */
	public static <T> void print(Collection<T> value) {
		if (value == null)
			System.out.println("图书馆无该种类书籍相关信息");
		else {
			Iterator iterator = value.iterator();
			while (iterator.hasNext())
				System.out.println(iterator.next());
		}

	}

	/**
	 * 展示书库
	 */
	public static void showBooks() {
		Collection<Set<BookItem>> bookItemSet = booksMap.values();
		Iterator<Set<BookItem>> iteratorSet = bookItemSet.iterator();
		while (iteratorSet.hasNext()) {
			print(iteratorSet.next());
		}
	}

	/**
	 * 搜索：依据书的类别
	 * 
	 * @param key
	 * @return
	 */
	public static Set<BookItem> searchBooks(String key) {
		Set<BookItem> bookItems = booksMap.get(key);
		return bookItems;
	}

}
