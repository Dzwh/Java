package basis;

import java.util.*;

/**
 * 不允许被继承，程序开始时应该初始化一个用户仓库，起到暂时容器的作用。 一个仓库类:存储用户的信息。 用Set存储：避免重复
 * 通过其add方法来加入新的用户。
 * 
 * @author 周文华
 *
 */
public final class UserStorage {
	private Set<User> userSet = new HashSet<>();

	/**
	 * 添加新用户
	 * 
	 * @param user
	 */
	public void addUser(User user) {
		userSet.add(user);

	}

	/**
	 * 注册时要用到
	 * 
	 * @param name
	 * @param password
	 * @return
	 */
	public boolean isExit(String name) {
		Iterator<User> iterator = userSet.iterator();
		while (iterator.hasNext()) {
			if (name.equals(iterator.next().getLoginInformation().getId()))
				return true;
		}
		return false;
	}

	public Set<User> getUserSet() {
		return userSet;
	}

	public void setUserSet(Set<User> userSet) {
		this.userSet = userSet;
	}

	@Override
	public String toString() {
		return "UserStorage [userSet=" + userSet + "]";
	}

	/**
	 * 打印用户信息,打印用户名,姓名，编号。
	 */

	public void showUsers() {
		String a="姓名";
		String b="学号";
		String c="帐号";
		for (User user : userSet) {
			System.out.println(String.format("%-23s%-25s%10s",a,b,c));
			System.out.println(String.format("%-20s%-15s%10s",user.getUserName(),user.getUserNo(), user.getLoginInformation().getId()));
		}

	}

}
