package AboutShopping;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.io.*;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 * 提供注册，登录,搜索商品，展示商品
 * 
 * @author 周文华
 * 
 *
 */
public class ShoppingMall {


	/**
	 * 
	 * 将用户对象序列化
	 * 
	 * @param userName.getName()
	 * @param userPassword
	 * @return User
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * 
	 * 
	 */

	public static int writerUser(String userName, String userPassword) throws IOException, ClassNotFoundException {
		User user = new User();

		try {
			FileInputStream fs = new FileInputStream("users.ser");
			ObjectInputStream is = new ObjectInputStream(fs);
			while (true) {

				user = (User) is.readObject();
				if (user.getUserId().equals(userName)) {
					is.close();
					return -1;
				}
			}
		} catch (EOFException e) {
			try {

				FileOutputStream out = new FileOutputStream("users.ser");
				ObjectOutputStream os = new ObjectOutputStream(out);
				os.writeObject(new User(userName, userPassword, new ShoppingCart()));
				os.close();
				return 1;// 成功注册
			} catch (Exception ex) {
				ex.printStackTrace();
				return 0;// 其他情况。

			}

		}

	}

	/**
	 * 解序列化
	 * 
	 * @param userId
	 * @param userPassword
	 * @param user
	 * @return boolean
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static User readUser(String userName, String userPassword) throws IOException, ClassNotFoundException {

		User user = null;

		try {
			FileInputStream fs = new FileInputStream("users.ser");
			ObjectInputStream is = new ObjectInputStream(fs);

			while (true) {
				user = (User) is.readObject();
				if (user.getUserId().equals(userName) && user.getUserPassword().equals(userPassword))
					return user;
			}
		} catch (EOFException ex) {
			return null;
		}
	}

	/**
	 * 提供搜索，输入商品种类搜索商品
	 * 
	 * @param key:商品种类
	 * @return keyList
	 */
	public static List<GoodsItem> searchGoodsAccordingToKind(String key) {
		Collection<GoodsItem> goodsItems = Storage.storageGoodsList.values();
		Iterator iterator = goodsItems.iterator();
		List<GoodsItem> keyList = new ArrayList<GoodsItem>();

		while (iterator.hasNext()) {
			GoodsItem goodsItem = (GoodsItem) iterator.next();
			Goods goods = goodsItem.getGood();
			if (goods.getGoodCategory().equals(key)) {

				keyList.add(goodsItem);
			}
		}
		return keyList;

	}

	/**
	 * 展示商品信息，有一个输出操作。后期可能修改。
	 * 
	 */
	public static void showStorage() {
		Collection<GoodsItem> goodsItems = Storage.storageGoodsList.values();
		Iterator<GoodsItem> iterator = goodsItems.iterator();

		while (iterator.hasNext()) {
			GoodsItem goodsItem = iterator.next();
			Goods goods = goodsItem.getGood();
			System.out.println("goodId=" + goods.getGoodId() + ", goodName=" + goods.getGoodName() + ", goodprice="
					+ goods.getGoodName() + ", goodCategory=" + goods.getGoodCategory() + ",goodsCount="
					+ goodsItem.getCount());
		}
	}

}
