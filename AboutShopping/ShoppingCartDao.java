package AboutShopping;

public interface ShoppingCartDao {

	public int addGoods(Integer goodsId);//增加商品；
	public void showGoosList();//展示购物车商品；
	public boolean deleteGoods(int goodsId);//删除商品；
	public boolean clearShoppingCart();//清空购物车；
	public int modifyGoods(int goodsId, int count);//修改商品；
	public double allTotalMoney();//计算购物车中所有商品价格；
}
