package AboutShopping;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.IOException;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;


public class Enter {
	public static User user;

	public void enter() {
		JFrame frame = new JFrame("����ϵͳ");
		JPanel panel = new JPanel();
		JButton registerButton = new JButton("ע��");
		JButton loginButton = new JButton("��¼");

		loginButton.addActionListener(new LoginLisener());
		registerButton.addActionListener(new RegisterListener());
		

		panel.add(registerButton);
		panel.add(loginButton);
		frame.setContentPane(panel);
		frame.setSize(300, 300);
		frame.setVisible(true);

	}

	class RegisterListener implements ActionListener {
		JTextField fieldName;
		JPasswordField fieldPassword;
		JFrame frame;
		JPanel panel;
		JButton ensureButton;
		JLabel userLabel;
		JLabel passwordLabel;
		// JLabel resultLabel;
		String userName;
		String userPassword;

		@Override
		public void actionPerformed(ActionEvent e) {
			frame = new JFrame("ע��");
			panel = new JPanel();
			ensureButton = new JButton("ȷ��");
			userLabel = new JLabel("�û�");
			passwordLabel = new JLabel("����");

			fieldName = new JTextField(20);
			fieldPassword = new JPasswordField(20);

			fieldName.addFocusListener(new userNameListener());
			fieldPassword.addFocusListener(new userPasswordListener());
			ensureButton.addActionListener(new ensureButtonListener());

			panel.add(userLabel);
			panel.add(fieldName);
			panel.add(passwordLabel);
			panel.add(fieldPassword);
			panel.add(ensureButton);

			frame.setContentPane(panel);
			frame.setSize(300, 300);
			frame.setVisible(true);

		}

		class ensureButtonListener implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					int result = ShoppingMall.writerUser(userName, userPassword);
					if (result == 1) {
						panel.add(new JLabel("ע��ɹ�"));
					}
					if (result == -1) {
						panel.add(new JLabel("���ʺ��Ѵ���"));
					}
					if (result == 0)
						panel.add(new JLabel("�ڲ�����"));
				

				} catch (ClassNotFoundException e1) {

					e1.printStackTrace();
				} catch (IOException e1) {

					e1.printStackTrace();
				}

			}

		}

		class userPasswordListener implements FocusListener {

			@Override
			public void focusGained(FocusEvent e) {

			}

			@Override
			public void focusLost(FocusEvent e) {
				userPassword = fieldPassword.getText();
				System.out.println(userPassword);

			}

		}

		class userNameListener implements FocusListener {

			@Override
			public void focusGained(FocusEvent e) {

			}

			@Override
			public void focusLost(FocusEvent e) {
				userName = fieldName.getText();
				System.out.println(userName);
			}

		}

	}

	class LoginLisener implements ActionListener {

		JTextField fieldName;
		JPasswordField fieldPassword;
		JFrame frame;
		JPanel panel;
		JButton ensureButton;
		JLabel userLabel;
		JLabel passwordLabel;
		String userName;
		String userPassword;

		@Override
		public void actionPerformed(ActionEvent e) {
			frame = new JFrame("��¼");
			panel = new JPanel();
			ensureButton = new JButton("ȷ��");
			userLabel = new JLabel("�û�");
			passwordLabel = new JLabel("����");
			fieldName = new JTextField(20);
			fieldPassword = new JPasswordField(20);

			panel.add(userLabel);
			panel.add(fieldName);
			panel.add(passwordLabel);
			panel.add(fieldPassword);
			panel.add(ensureButton);

			fieldName.addFocusListener(new userNameListener());
			fieldPassword.addFocusListener(new userPasswordListener());
			ensureButton.addActionListener(new ensureButtonListener());

			frame.setContentPane(panel);
			frame.setSize(300, 300);
			frame.setVisible(true);

		}

		class ensureButtonListener implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if(ShoppingMall.readUser(userName, userPassword)==null)
						panel.add(new JLabel("�ʺ����벻ƥ��"));
					else {
						user=ShoppingMall.readUser(userName, userPassword);
						panel.add(new JLabel("��¼�ɹ�"));
						new Start().start();
						
					}
					
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}

			}

		}

		class userNameListener implements FocusListener {

			@Override
			public void focusGained(FocusEvent e) {

			}

			@Override
			public void focusLost(FocusEvent e) {
				userName = fieldName.getText();
				System.out.println(userName);
			}

		}

		class userPasswordListener implements FocusListener {

			@Override
			public void focusGained(FocusEvent e) {

			}

			@Override
			public void focusLost(FocusEvent e) {
				userPassword = fieldPassword.getText();
				System.out.println(userPassword);

			}

		}

	}

	
	
	
	
}
