package AboutShopping;

import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * 仓库类
 */
public final class Storage {
	

	//定义为static
	public static Map<Integer, GoodsItem> storageGoodsList = new TreeMap<Integer, GoodsItem>();

	// 对仓库初始化
	static {
		storageGoodsList.put(001, new GoodsItem(new Goods(001, "book1", 23.3, "book"), 5));
		storageGoodsList.put(002, new GoodsItem(new Goods(002, "book2", 23.3, "book"), 6));
		storageGoodsList.put(003, new GoodsItem(new Goods(003, "book3", 23.3, "book"), 7));
		storageGoodsList.put(004, new GoodsItem(new Goods(004, "book4", 23.3, "book"), 8));
		storageGoodsList.put(005, new GoodsItem(new Goods(005, "pants", 23.3, "clothing"), 8));
		storageGoodsList.put(006, new GoodsItem(new Goods(006, "T-Shirt", 23.3, "clothing"), 4));
		storageGoodsList.put(007, new GoodsItem(new Goods(007, "coat", 23.3, "clothing"), 2));
		storageGoodsList.put(010, new GoodsItem(new Goods(010, "dress", 23.3, "clothing"), 6));

	}
	
	

	// 增加商品到仓库
	private void addGoods(Goods goods) {
		int goodsId = goods.getGoodId();
		if (storageGoodsList.containsKey(goodsId)) {
			GoodsItem goodsItem = storageGoodsList.get(goodsId);
			goodsItem.setCount(goodsItem.getCount() + 1);
		} else {
			storageGoodsList.put(goodsId, new GoodsItem(goods, 1));
		}
	}

	// 展示仓库商品
	public static void showStorage() {
		Collection<GoodsItem> goodsItems = storageGoodsList.values();
		Iterator<GoodsItem> iterator = goodsItems.iterator();

		while (iterator.hasNext()) {
			GoodsItem goodsItem = iterator.next();
			Goods goods = goodsItem.getGood();
			System.out.println("goodId=" + goods.getGoodId() + ", goodName=" + goods.getGoodName() + ", goodprice=" + goods.getGoodName() + ", goodCategory="
					+ goods.getGoodCategory() +",goodsCount="+goodsItem.getCount());
		}
	}

	
	
	//转为商城的行为
	// 输入商品种类搜索商品
/*	public static void searchGoodsAccordingToKind(String key) {
		Collection<GoodsItem> goodsItems = Storage.storageGoodsList.values();
		Iterator iterator = goodsItems.iterator();
		int flag=0;
		
		while (iterator.hasNext()) {
			GoodsItem goodsItem = (GoodsItem) iterator.next();
			Goods goods = goodsItem.getGood();
			if (goods.getGoodCategory().equals(key)) {
				flag=1;
				System.out.println(goodsItem.toString());
			}
		}
		if(flag==0) {
			System.out.println("本商城没有该种类商品");
		}
	}*/

}
