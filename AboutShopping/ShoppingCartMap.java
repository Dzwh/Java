package AboutShopping;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * 
 * @author 周文华
 *
 */

public class ShoppingCartMap implements ShoppingCartDao{
	
		private Map<Integer, GoodsItem> goodsList = new TreeMap<Integer, GoodsItem>();

		// 添加商品到购物车
		public int addGoods(Integer goodsId) {
			boolean b = Storage.storageGoodsList.containsKey(goodsId);
			if (b == true) {
				GoodsItem storageGoodsItem = Storage.storageGoodsList.get(goodsId);
				if (goodsList.containsKey(goodsId)) {
					if (goodsList.get(goodsId).getCount() < storageGoodsItem.getCount()) {
						GoodsItem temp = goodsList.get(goodsId);
						temp.setCount(temp.getCount() + 1);
						return 1;
					} else
						return 0;
				} else {
					goodsList.put(goodsId, new GoodsItem(storageGoodsItem.getGood(), 1));
					return 1;
				}
			} else {
				return -1;
			}
		}

		// 展示购物车内容
		public void showGoosList() {
			Collection<GoodsItem> goodsItems = goodsList.values();
			Iterator<GoodsItem> iterator = goodsItems.iterator();

			while (iterator.hasNext()) {
				GoodsItem goodsItem = iterator.next();
				Goods goods = goodsItem.getGood();
				int goodsId = goods.getGoodId();
				int stock = Storage.storageGoodsList.get(goodsId).getCount();
				System.out.println(String.format("GoodsId:%-4dGoodsName:%-10sGoodCategory:%-10sGoodsCount:%d/%-4dTotalMoney:%-5.3f元",
						goods.getGoodId(), goods.getGoodName(), goods.getGoodCategory(),goodsItem.getCount(), stock, goodsItem.totalMoney()));
			}

		}

		// 删除购物车中的商品
		public boolean deleteGoods(int goodsId) {
			if (goodsList.containsKey(goodsId)) {
				goodsList.remove(goodsId);
				return true;
			} else
				return false;
		}

		// 清空购物车
		public boolean clearShoppingCart() {
			goodsList.clear();
			return goodsList.isEmpty();
		}

		// 对购物车中的商品进行修改
		public int modifyGoods(int goodsId, int count) {

			if (goodsList.containsKey(goodsId)) {
				if (count > 0 && count <= Storage.storageGoodsList.get(goodsId).getCount()) {
					GoodsItem goodsItem = goodsList.get(goodsId);
					goodsItem.setCount(count);
					return 1;
				}
				if (count == 0) {
					deleteGoods(goodsId);
					return 1;
				}
				if (count > Storage.storageGoodsList.get(goodsId).getCount())
					return 0;
			}
			return -1;
		}

		// 计算购物车中所有商品的总价
		public double allTotalMoney() {
			double allmoney = 0;
			Collection<GoodsItem> goodsItems = goodsList.values();
			Iterator<GoodsItem> itrator = goodsItems.iterator();

			while (itrator.hasNext()) {
				GoodsItem goodsItem = itrator.next();
				double money = goodsItem.totalMoney();
				allmoney += money;
			}
			return allmoney;
		}

	}


