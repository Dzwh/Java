package AboutShopping;

import java.util.*;

/**
 * 
 * @author 周文华
 *
 */

public class ShoppingCartList implements ShoppingCartDao {

	private List<GoodsItem> goodsList = new ArrayList<GoodsItem>();

	@Override
	public int addGoods(Integer goodsId) {
		boolean b = Storage.storageGoodsList.containsKey(goodsId);
		if (b) {
			int flag = 0;
			Goods goods=null;
			GoodsItem cartGoodsItem = null;
			GoodsItem storageGoodsItem = Storage.storageGoodsList.get(goodsId);
			for (int i = 0; i < goodsList.size(); i++) {
				cartGoodsItem = goodsList.get(i);
				goods = cartGoodsItem.getGood();
				if (goods.getGoodId().equals(goodsId)) {
					flag = 1;
				}
			}
			if (flag == 0) {
				goodsList.add(new GoodsItem(storageGoodsItem.getGood(), 1));
				return 1;
			} else {
				if (cartGoodsItem.getCount() < storageGoodsItem.getCount()) {
					cartGoodsItem.setCount(cartGoodsItem.getCount() + 1);
					return 1;// 成功添加。
				} else
					return 0;// 库存不足
			}

		} else
			return -1;// 仓库没有该商品
	}

	@Override
	public void showGoosList() {

		Iterator<GoodsItem> iterator = goodsList.iterator();

		while (iterator.hasNext()) {
			GoodsItem goodsItem = iterator.next();
			Goods goods = goodsItem.getGood();
			int goodsId = goods.getGoodId();
			int stock = Storage.storageGoodsList.get(goodsId).getCount();
			System.out.println(
					String.format("GoodsId:%-4dGoodsName:%-10sGoodCategory:%-10sGoodsCount:%d/%-4dTotalMoney:%-5.3f元",
							goods.getGoodId(), goods.getGoodName(), goods.getGoodCategory(), goodsItem.getCount(),
							stock, goodsItem.totalMoney()));

		}

	}

	@Override
	public boolean deleteGoods(int goodsId) {
		
		int flag=0;
		int index = 0;
		GoodsItem goodsItem=null;
		Goods goods=null;
		
		for(int i=0;i<goodsList.size();i++) {
			goodsItem=goodsList.get(i);
			goods=goodsItem.getGood();
			
			if(goods.getGoodId().equals(goodsId)) {
				flag=1;
				index=i;
			}
		}
		
		if(flag==0) {
			return false;//购物车不含该商品；
			
		}else {
			goodsList.remove(index);
			return true;
		}

		
	}

	@Override
	public boolean clearShoppingCart() {

		goodsList.clear();
		return goodsList.isEmpty();
	}

	@Override
	public int modifyGoods(int goodsId, int count) {
		
		int flag=0;
		int index = 0;
		GoodsItem goodsItem=null;
		Goods goods=null;
		
		for(int i=0;i<goodsList.size();i++) {
			goodsItem=goodsList.get(i);
			goods=goodsItem.getGood();
			
			if(goods.getGoodId().equals(goodsId)) {
				flag=1;
				index=i;
			}
		}
		
		if(flag!=0) {
			if(count>0&&count<=Storage.storageGoodsList.get(goodsId).getCount()) {
				 goodsItem=goodsList.get(index);
				 goodsItem.setCount(count);
				 return 1;//修改成功
			}
			if (count == 0) {
				deleteGoods(goodsId);
				return 1;
			}
			if (count > Storage.storageGoodsList.get(goodsId).getCount())
				return 0;//库存不足
		}
			return -1;////购物车不含该商品；
	

		
	}

	@Override
	public double allTotalMoney() {
		
		double allMoney = 0;

		Iterator iterator = goodsList.iterator();
		while (iterator.hasNext()) {
			GoodsItem goodsItem = (GoodsItem) iterator.next();
			double money = goodsItem.totalMoney();
			allMoney += money;
		}

		return allMoney;
	}

}
