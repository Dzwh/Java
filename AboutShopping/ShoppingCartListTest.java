package AboutShopping;

import java.util.*;

public class ShoppingCartListTest {
	public static void main(String[] args) {
		ShoppingCartList cart = new ShoppingCartList();
		Scanner sc = new Scanner(System.in);

		Storage.showStorage();

		System.out.println("菜单");
		System.out.println("1.添加商品");
		System.out.println("2.修改商品");
		System.out.println("3.删除商品");
		System.out.println("4.清空购物车");
		System.out.println("5.查看购物车");
		System.out.println("0.退出程序");

	out:	while (true) {
			String input = sc.next();

			switch (input) {
			case "1":
				System.out.println("请输入商品Id");
				int result1 = cart.addGoods(sc.nextInt());
				if (result1 == 1)
					System.out.println("成功添加");
				else if (result1 == 0)
					System.out.println("库存不足");
				else
					System.out.println("仓库不存在该商品");
				break;

			case "2":
				System.out.println("请输入商品Id及数量");
				int result2 = cart.modifyGoods(sc.nextInt(), sc.nextInt());
				if (result2 == 1)
					System.out.println("修改成功");
				else if (result2 == 0)
					System.out.println("库存不足");
				else
					System.out.println("购物车不存在该商品");
				break;

			case "3":
				System.out.println("请输入商品Id");
				boolean result3 = cart.deleteGoods(sc.nextInt());

				if (result3)
					System.out.println("删除成功");
				else
					System.out.println("购物车不存在该商品");
				break;

			case "4":
				boolean result4 = cart.clearShoppingCart();
				if (result4)
					System.out.println("购物车已清空");
				else
					System.out.println("清空失败");
				break;
			case "5":
				cart.showGoosList();
				System.out.println(cart.allTotalMoney());
				break;
			default:
				break out;

			}
		}

	}

}
