package javalearn2;

import java.util.Scanner;
import java.util.*;

class Rational {
	private int numerator;
	private int denominator;
	private float number;


	
	public Rational(float number) {
		super();
		this.number = number;
	}

	public  Rational() {};

	//将用户输入的数转化为分数
	static void convertTofractions(Rational num) {
	
		num.numerator = (int) num.number;
		num.denominator = 1;
		while (num.number != (int) num.number) {
			num.denominator *= 10;
			num.number *= 10;
			num.numerator = (int) num.number;

		}
	}
   //化简
	static Rational simplify(Rational tempResult) {

		int x = tempResult.numerator;
		int y = tempResult.denominator;

		if (x < y) {
			int temp = x;
			x = y;
			y = temp;
		}
		int r = 1;
		while (r != 0) {
			r = x % y;
			x = y;
			y = r;
		}
		tempResult.numerator = tempResult.numerator / x;
		tempResult.denominator = tempResult.denominator / x;
		return tempResult;
	}

	static Rational addition(Rational num1, Rational num2) {

		Rational tempResult = new Rational();
		tempResult.numerator = num1.numerator * num2.denominator + num2.numerator * num1.denominator;
		tempResult.denominator = num1.denominator * num2.denominator;
		return simplify(tempResult);

	}

	static Rational subtraction(Rational num1, Rational num2) {

		Rational tempResult = new Rational();
		tempResult.numerator = num1.numerator * num2.denominator - num2.numerator * num2.denominator;
		tempResult.denominator = num1.denominator * num2.denominator;
		return simplify(tempResult);

	}

	static Rational multiplication(Rational num1, Rational num2) {

		Rational tempResult = new Rational();
		tempResult.numerator = num1.numerator * num2.numerator;
		tempResult.denominator = num1.denominator * num2.denominator;
		return simplify(tempResult);

	}

	static Rational division(Rational num1, Rational num2) {

		Rational tempResult = new Rational();
		tempResult.numerator = num1.numerator * num2.denominator;
		tempResult.denominator = num1.denominator * num2.numerator;
		return simplify(tempResult);

	}

	public int getNumerator() {
		return numerator;
	}

	public void setNumerator(int numerator) {
		this.numerator = numerator;
	}

	public int getDenominator() {
		return denominator;
	}

	public void setDenominator(int denominator) {
		this.denominator = denominator;
	}
}

public class RationalTest {

	public static void choice() {
		System.out.println("please select:");
		System.out.println("addition:1");
		System.out.println("subtraction:2");
		System.out.println("multiplication:3");
		System.out.println("division:4");
		System.out.println("exit:0");
	}

	public static void main(String[] args) {

		System.out.println("201621123008  周文华");
		
		Scanner in = new Scanner(System.in);
		Rational num1 ;
		Rational num2 ;
		Rational result = new Rational();

		while (true) {
			System.out.print("Please enter the value of num1:");
			num1=new Rational(in.nextFloat());
			Rational.convertTofractions(num1);
			System.out.print("Please enter the value of num2:");
			num2=new Rational(in.nextFloat());
			Rational.convertTofractions(num2);
			choice();

			out: while (true) {

				int flag = in.nextInt();

				switch (flag) {
				case 1:
					result = Rational.addition(num1, num2);
					break;
				case 2:
					result = Rational.subtraction(num1, num2);

					break;
				case 3:
					result = Rational.multiplication(num1, num2);
					break;
				case 4:
					result = Rational.division(num1, num2);
					break;
				default:
					break out;
				}

				System.out.println("The fraction results:  " + result.getNumerator() + "/" + result.getDenominator());
				float temp = result.getNumerator() / (float) result.getDenominator();
				System.out.println("The decimal result:   " + temp);

			}
		}
	}
}
