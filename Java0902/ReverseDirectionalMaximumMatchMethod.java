package Java0902;

import java.util.*;

/**
 * 逆向最大分词
 * 
 * @author 周文华 201621123008
 *
 */
public class ReverseDirectionalMaximumMatchMethod {
	public static void main(String[] args) {

		Set<String> wordsSet = new HashSet<>();
		Scanner sc = new Scanner(System.in);
		String[] words = sc.nextLine().split("\\s+");

		for (String str : words) {
			wordsSet.add(str);
		}

		while (true) {
			List<String> newList = new ArrayList<>();
			String sentence = sc.nextLine();
			int front = 0;

			while (sentence.length() != 0) {
				int maxLength = sentence.length();
				String temp = sentence.substring(front, maxLength);
				if (wordsSet.contains(temp)) {
					newList.add(temp);
					sentence = sentence.replace(temp, "");
					front = 0;
				} else {
					front += 1;
					if(front>=maxLength)
						break;
				}
			}

			if (newList.size() > 0) {
				System.out.print(newList.get(newList.size() - 1));
				if (newList.size() > 2)
					for (int i = newList.size() - 2; i >= 0; i--)
						System.out.print(" " + newList.get(i));
			}
			System.out.println();

		}

	}

}
