package Java0902;

public class Main1 {
	public static <T extends Comparable<T>> T min(T[] a) {
		T smallest=a[0];
		for(int i=1;i<a.length;i++) {
			if((smallest.compareTo(a[i])>0))
				smallest=a[i];
		}
		return smallest;
	}

}
