package Java0902;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
/**
 * 作业泛型方法
 * @author 周文华
 *
 */
class User implements Comparable<User> {
	private int age;

	public User(int age) {
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	@Override
	public int compareTo(User o) {

		return age - o.age;
	}

	@Override
	public String toString() {
		return "User [age=" + age + "]";
	}

}

class StuUser extends User {
	private String no;

	public StuUser(int age, String no) {
		super(age);
		this.no = no;
	}

	public String getNo() {
		return no;
	}

	@Override
	public String toString() {
		return "StuUser [no=" + no + ", toString()=" + super.toString() + "]";
	}

}

class UserReverseComparator implements Comparator<User> {

	@Override
	public int compare(User o1, User o2) {
		return o2.getAge() - o1.getAge();
	}

}

class StuUserComparator implements Comparator<StuUser> {

	@Override
	public int compare(StuUser o1, StuUser o2) {
		return o1.getNo().compareTo(o2.getNo());
	}

}

public class GenericMain {

	public static <T extends Comparable<T>> T max(List<T> list) {
		T max = list.get(0);

		for (int i = 0; i < list.size(); i++) {
			if (max.compareTo(list.get(i)) < 0)
				max = list.get(i);
		}
		return max;

	}
	
	public static <T extends StuUser> T max1(List<T> list) {
		T max=list.get(0);
		for (int i = 0; i < list.size(); i++) {
			if (max.compareTo(list.get(i)) < 0)
				max = list.get(i);
		}
		return max;
		
		
	}
	
	public static <T extends User> int myCompare(T o1,T o2,Comparator<T> c) {
		return c.compare(o1, o2);
		
	}

	public static void main(String[] args) {

		List<String> strList = new ArrayList<>();
		strList.add("1");
		strList.add("2");
		strList.add("3");
		strList.add("4");
		System.out.println(max(strList));

		List<Integer> intList = new ArrayList<>();
		intList.add(1);
		intList.add(2);
		intList.add(3);
		intList.add(4);
		System.out.println(max(intList));
		
		List<StuUser> stuList=new ArrayList<>();
		stuList.add(new StuUser(10, "10"));
		stuList.add(new StuUser(10, "11"));
		stuList.add(new StuUser(13, "20"));
		stuList.add(new StuUser(13, "11"));
		System.out.println(max1(stuList));
		
		List<Object> objList=new ArrayList<>();
		objList.add(stuList);
		System.out.println(max1(stuList));
		
		User user1=new User(5);
		User user2=new User(20);
		System.out.println(myCompare(user1, user2, new UserReverseComparator()));
		
		StuUser stu1=new StuUser(10, "10");
		StuUser stu2=new StuUser(10, "12");
		System.out.println(myCompare(stu1, stu2, new StuUserComparator()));
		
		

	}
}